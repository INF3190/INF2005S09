/**
 * @author Johnny Tsheke
 */
$(document).ready(function(){
	var can=$("#canvas1");
	var x = can[0].width / 2;//coord. x du centre
	var y= can[0].height / 2; //coord. y du centre
	var rayon=50;
	var contxt=can[0].getContext("2d");// !tableau avec jQuery
	//contxt.arc(x,y,rayon,Math.PI/2,(3*Math.PI)/2);//angl début et fin en radians
	contxt.arc(x,y,rayon,Math.PI/2,(3*Math.PI)/2,true);//sens contraire ->trigo
	//contxt.arc(x,y,rayon,0,(2*Math.PI),true)
	contxt.stroke();
});