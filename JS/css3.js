/**
 * @author Johnny Tsheke
 */

function debut()
{ 
var image=$("img").eq(0);
 $(image).removeAttr("class");
	$(image).attr({"class":"debut"});
	$(image).click(function(){ 
		basGauche();
	});
}

function basGauche()
{var image=$("img").eq(0);
	$(image).removeAttr("class");
	$(image).attr({"class":"basg"});
	$(image).click(function(){
		basDroit();
	});
}

function basDroit()
{var image=$("img").eq(0);
	$(image).removeAttr("class");
	$(image).attr({"class":"basd"});
	$(image).click(function(){
		hautDroit();
	});
}

function hautDroit()
{var image=$("img").eq(0);
	$(image).removeAttr("class");
	$(image).attr({"class":"hautd"});
	$(image).click(function(){
		debut();
	});
}

$(document).ready(function(){ 
	//var image=$("img").eq(0);
	debut();
});
